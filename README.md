# RentIt App

[Check it out live!](https://rentitapartments-app.herokuapp.com/)

## Development

Run `yarn && yarn dev`

## Production version

Run `yarn build & yarn start`
