import { Button, CircularProgress, TextField, Typography } from "@mui/material";
import { Box } from "@mui/system";
import * as React from "react";
import { useHistory } from "react-router-dom";
import { saveSession } from "../../components/auth";
import {
  authActions,
  LoginResponse,
  selectUser,
} from "../../store/auth/auth.slice";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { REGISTER_ENDPOINT } from "../../utils";
import { apiClient } from "../../utils/apiClient";

export function Register() {
  const [name, setName] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [loading, setLoading] = React.useState(false);
  const dispatch = useAppDispatch();
  const history = useHistory();
  const loggedUser = useAppSelector(selectUser);

  React.useEffect(() => {
    if (loggedUser) {
      history.push("/apartments");
    }
  }, [loggedUser, history]);

  const register = React.useCallback(
    async (event: React.FormEvent) => {
      try {
        setLoading(true);
        const { token, user } = (
          await apiClient.post<any, { data: LoginResponse }>(
            REGISTER_ENDPOINT,
            {
              email,
              name,
              password,
            },
          )
        ).data;
        if (token && user) {
          saveSession({ token, user });
          dispatch(authActions.setUserAndToken({ token, user }));
          setLoading(false);
        }
      } catch (error) {
        setLoading(false);
        console.log({ error });
      }
    },
    [dispatch, email, name, password],
  );
  return loading ? (
    <Box sx={{ display: "flex" }}>
      <CircularProgress />
    </Box>
  ) : (
    <Box sx={{ display: "flex", justifyContent: "center" }}>
      <Box>
        <Typography variant="h5">Register</Typography>
        <Box sx={{ marginTop: 3 }}>
          <TextField
            required
            label="Name"
            variant="filled"
            value={name}
            onChange={({ target: { value } }) => setName(value)}
          />
        </Box>
        <Box sx={{ marginTop: 3 }}>
          <TextField
            required
            label="Email"
            variant="filled"
            value={email}
            onChange={({ target: { value } }) => setEmail(value)}
          />
        </Box>
        <Box sx={{ marginTop: 3 }}>
          <TextField
            label="Password"
            type="password"
            variant="filled"
            value={password}
            onChange={({ target: { value } }) => setPassword(value)}
          />
        </Box>
        <Box
          sx={{
            marginTop: 3,
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Button variant="contained" onClick={register}>
            Register
          </Button>
        </Box>
      </Box>
    </Box>
  );
}
