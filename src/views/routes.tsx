import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { PrivateRoute } from "../components";
import { Apartments } from "./apartments";
import { ApartmentCreate } from "./apartments/create";
import { ApartmentEdit } from "./apartments/edit";
import { Login } from "./login";
import { Register } from "./register";
import { Users } from "./users";
import { UserCreate } from "./users/create";
import { UserEdit } from "./users/edit";

export function Routes() {
  return (
    <Switch>
      <Route path="/login">
        <Login />
      </Route>
      <Route path="/register">
        <Register />
      </Route>
      <PrivateRoute roles={["admin"]} path="/users" exact={true}>
        <Users />
      </PrivateRoute>
      <PrivateRoute roles={["admin"]} path="/users/create" exact={true}>
        <UserCreate />
      </PrivateRoute>
      <PrivateRoute roles={["admin"]} path="/users/edit/:id" exact={true}>
        <UserEdit />
      </PrivateRoute>
      <Route path="/apartments" exact={true}>
        <Apartments />
      </Route>
      <PrivateRoute
        roles={["admin", "realtor"]}
        path="/apartments/create"
        exact={true}
      >
        <ApartmentCreate />
      </PrivateRoute>
      <PrivateRoute
        roles={["admin", "realtor"]}
        path="/apartments/edit/:id"
        exact={true}
      >
        <ApartmentEdit />
      </PrivateRoute>
      <Route path="/">
        <Redirect to="/apartments" />
      </Route>
    </Switch>
  );
}
