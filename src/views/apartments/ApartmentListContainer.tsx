import AddCircleIcon from "@mui/icons-material/AddCircle";
import { Button, List, Typography } from "@mui/material";
import { Box } from "@mui/system";
import * as React from "react";
import { useHistory } from "react-router-dom";
import { ApartmentCard } from "../../components/apartment-card";
import { selectUser } from "../../store/auth/auth.slice";
import { useAppSelector } from "../../store/hooks";
import { Apartment, roles } from "../../types";
import { ACCESS } from "../../utils/permissions";

interface Props {
  apartments: Apartment[];
  selected: number;
  setSelected: React.Dispatch<React.SetStateAction<number>>;
}

export function ApartmentsListContainer({
  apartments,
  selected,
  setSelected,
}: Props) {
  const history = useHistory();
  const loggedUser = useAppSelector(selectUser);
  const hasWritePermissions = ACCESS.APARTMENTS.WRITE.includes(
    loggedUser?.role as roles,
  );

  return (
    <Box sx={{ maxHeight: "100vh", overflow: "auto" }}>
      {hasWritePermissions && (
        <Button
          onClick={() => history.push("/apartments/create")}
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            width: "100%",
            marginTop: 4,
          }}
        >
          <Typography variant="h6"> Apartments</Typography>
          <AddCircleIcon
            sx={{
              fontSize: 40,
            }}
          />
        </Button>
      )}
      <List>
        {apartments.map((apartment) => (
          <ApartmentCard
            key={apartment.id}
            apartment={apartment}
            expanded={apartment.id === selected}
            onSelect={() =>
              apartment.id === selected
                ? setSelected(-1)
                : setSelected(apartment.id)
            }
            hasWritePermissions={hasWritePermissions}
          />
        ))}
      </List>
    </Box>
  );
}
