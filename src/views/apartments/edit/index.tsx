import { Button, Typography } from "@mui/material";
import { Box } from "@mui/system";
import * as React from "react";
import { useParams } from "react-router";
import { useHistory } from "react-router-dom";
import {
  fetchApartments,
  selectApartmentById,
} from "../../../store/apartment/apartment.slice";
import { useAppDispatch, useAppSelector } from "../../../store/hooks";
import {
  ApartmentCreationParams,
  ApartmentReadAttributes,
  ApartmentUpdationKeys,
} from "../../../types";
import { APARTMENTS_ENDPOINT } from "../../../utils";
import { apiClient } from "../../../utils/apiClient";
import { ApartmentForm } from "../shared";

export function ApartmentEdit() {
  const { id } = useParams<{ id: string }>();
  const dispatch = useAppDispatch();
  const history = useHistory();
  const selectedApartment = useAppSelector(selectApartmentById(Number(id)));
  const [apartment, setApartment] =
    React.useState<null | ApartmentReadAttributes>(null);

  React.useEffect(() => {
    if (apartment === null && selectedApartment?.id) {
      setApartment(selectedApartment);
    }
  }, [apartment, selectedApartment]);

  React.useEffect(() => {
    dispatch(fetchApartments());
  }, [dispatch]);

  const onChangeField = React.useCallback(
    (field: ApartmentUpdationKeys, value: any) => {
      setApartment({
        ...(apartment || {}),
        [field]: value,
      } as ApartmentReadAttributes);
    },
    [setApartment, apartment],
  );

  const onSubmit = React.useCallback(async () => {
    const { id, dateAdded, createdAt, updatedAt, ...apartmentData } =
      apartment as ApartmentReadAttributes;
    await apiClient.patch(`${APARTMENTS_ENDPOINT}/${id}`, apartmentData);
    history.push("/apartments");
  }, [apartment, history]);

  return (
    <Box>
      <div>
        <Button onClick={() => history.push("/apartments")}>
          Back To Apartments
        </Button>
      </div>
      <Typography variant="h3">Update Apartment</Typography>
      <Box>
        <ApartmentForm
          apartment={apartment as ApartmentCreationParams}
          onChangeField={onChangeField}
        />
        <Button variant="contained" onClick={onSubmit}>
          Update
        </Button>
      </Box>
    </Box>
  );
}
