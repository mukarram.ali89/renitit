import { MenuItem, Switch, TextField, TextFieldProps } from "@mui/material";
import { Box } from "@mui/system";
import * as React from "react";
import { GeolocationAutocomplete } from "../../../components/geolocation-autocomplete";
import { useAppDispatch, useAppSelector } from "../../../store/hooks";
import { fetchUsers, selectUsers } from "../../../store/user/user.slice";
import { Apartment, ApartmentCreationKeys } from "../../../types";

interface FormProps {
  type?: "create" | "edit";
  apartment: Partial<Apartment>;
  onChangeField: (field: ApartmentCreationKeys, value: any) => void;
}

export function ApartmentForm(props: FormProps) {
  const { apartment, onChangeField, type = "edit" } = props;
  const dispatch = useAppDispatch();
  const realtors = useAppSelector(selectUsers).filter(
    (u) => u.role === "realtor",
  );
  React.useEffect(() => {
    dispatch(fetchUsers());
  }, [dispatch]);

  const commonTextFieldProps: Partial<TextFieldProps> = {
    required: true,
    InputLabelProps: {
      shrink: true,
    },
    sx: {
      marginTop: 2,
      width: 300,
    },
  };

  return (
    <Box sx={{ mb: 1 }}>
      <div>
        <TextField
          {...commonTextFieldProps}
          type="number"
          label="AreaSize"
          value={apartment?.areaSize || 0}
          onChange={({ target: { value } }) => onChangeField("areaSize", value)}
        />
      </div>
      {type === "edit" && (
        <div>
          <Switch
            checked={apartment?.available || false}
            onChange={() => onChangeField("available", !apartment?.available)}
          />
        </div>
      )}
      <div>
        <TextField
          {...commonTextFieldProps}
          label="Description"
          value={apartment?.description || ""}
          onChange={({ target: { value } }) =>
            onChangeField("description", value)
          }
        />
      </div>
      <div>
        <TextField
          {...commonTextFieldProps}
          type="number"
          label="Floor"
          value={apartment?.floor || 0}
          onChange={({ target: { value } }) => onChangeField("floor", value)}
        />
      </div>
      <GeolocationAutocomplete
        setCoordinates={(location) => onChangeField("geolocation", location)}
        defaultTitle={apartment?.geolocation?.title}
      />
      <div>
        <TextField
          {...commonTextFieldProps}
          type="number"
          label="NumberOfRooms"
          value={apartment?.numberOfRooms || 0}
          onChange={({ target: { value } }) =>
            onChangeField("numberOfRooms", value)
          }
        />
      </div>
      <div>
        <TextField
          {...commonTextFieldProps}
          type="number"
          label="PricePerMonth"
          value={apartment?.pricePerMonth || 0}
          onChange={({ target: { value } }) =>
            onChangeField("pricePerMonth", value)
          }
        />
      </div>
      <div>
        <TextField
          {...commonTextFieldProps}
          label="Realtor"
          select
          value={apartment?.realtorId || -1}
          onChange={({ target: { value } }) =>
            onChangeField("realtorId", value)
          }
        >
          {realtors.map((realtor) => (
            <MenuItem key={realtor.id} value={realtor.id}>
              {realtor.name}
            </MenuItem>
          ))}
        </TextField>
      </div>
    </Box>
  );
}
