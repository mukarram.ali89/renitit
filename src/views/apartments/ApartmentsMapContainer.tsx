import { Box } from "@mui/system";
import * as React from "react";
import ApartmentsMap from "../../components/map";
import { Apartment } from "../../types";

interface Props {
  apartments: Apartment[];
  selected: number;
  setSelected: React.Dispatch<React.SetStateAction<number>>;
}

export function ApartmentsMapContainer({
  apartments, selected, setSelected
}: Props) {
  return (
    <Box>
      <ApartmentsMap
        apartments={apartments}
        selected={selected}
        setSelected={setSelected}
      />
    </Box>
  );
}
