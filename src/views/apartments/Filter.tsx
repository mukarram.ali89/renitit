import {
  Button,
  Divider,
  Grid,
  Stack,
  TextField,
  TextFieldProps,
  Typography,
} from "@mui/material";
import * as React from "react";
import { ApartmentReadAttributes } from "../../types";

const defaultFilterInputProps: Partial<TextFieldProps> = {
  sx: { width: 60 },
  type: "number",
  variant: "standard",
};

const fallback = {
  areaSize: 0,
  pricePerMonth: 0,
  numberOfRooms: 0,
} as ApartmentReadAttributes;

export function Filter({
  apartments,
  setFilteredApartments,
}: {
  apartments: ApartmentReadAttributes[];
  setFilteredApartments: React.Dispatch<
    React.SetStateAction<ApartmentReadAttributes[] | null>
  >;
}) {
  const {
    realMinSize,
    realMaxSize,
    realMinPrice,
    realMaxPrice,
    realMinRoom,
    realMaxRoom,
  } = React.useMemo(
    () => ({
      realMinSize: apartments.reduce(
        (a, b) => (a.areaSize < b.areaSize ? a : b),
        fallback,
      ).areaSize,
      realMaxSize: apartments.reduce(
        (a, b) => (a.areaSize > b.areaSize ? a : b),
        fallback,
      ).areaSize,
      realMinPrice: apartments.reduce(
        (a, b) => (a.pricePerMonth < b.pricePerMonth ? a : b),
        fallback,
      ).pricePerMonth,
      realMaxPrice: apartments.reduce(
        (a, b) => (a.pricePerMonth > b.pricePerMonth ? a : b),
        fallback,
      ).pricePerMonth,
      realMinRoom: apartments.reduce(
        (a, b) => (a.numberOfRooms < b.numberOfRooms ? a : b),
        fallback,
      ).numberOfRooms,
      realMaxRoom: apartments.reduce(
        (a, b) => (a.numberOfRooms > b.numberOfRooms ? a : b),
        fallback,
      ).numberOfRooms,
    }),
    [apartments],
  );

  const [minSize, setMinSize] = React.useState(0);
  const [maxSize, setMaxSize] = React.useState(0);
  const [minPrice, setMinPrice] = React.useState(0);
  const [maxPrice, setMaxPrice] = React.useState(0);
  const [minRoom, setMinRoom] = React.useState(0);
  const [maxRoom, setMaxRoom] = React.useState(0);

  React.useEffect(() => {
    if (apartments.length > 0) {
      setMinSize(realMinSize);
      setMaxSize(realMaxSize);
      setMinPrice(realMinPrice);
      setMaxPrice(realMaxPrice);
      setMinRoom(realMinRoom);
      setMaxRoom(realMaxRoom);
    }
  }, [
    apartments,
    realMaxPrice,
    realMaxRoom,
    realMaxSize,
    realMinPrice,
    realMinRoom,
    realMinSize,
  ]);

  const onReset = React.useCallback(() => {
    setMinSize(realMinSize);
    setMaxSize(realMaxSize);
    setMinPrice(realMinPrice);
    setMaxPrice(realMaxPrice);
    setMinRoom(realMinRoom);
    setMaxRoom(realMaxRoom);
  }, [
    realMaxPrice,
    realMaxRoom,
    realMaxSize,
    realMinPrice,
    realMinRoom,
    realMinSize,
  ]);

  function updateFilter(
    setter: React.Dispatch<React.SetStateAction<number>>,
  ):
    | React.ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>
    | undefined {
    return ({ target: { value } }) => setter(Number(value));
  }

  React.useEffect(() => {
    setFilteredApartments(
      apartments.filter(
        ({ areaSize, pricePerMonth, numberOfRooms }) =>
          areaSize >= minSize &&
          areaSize <= maxSize &&
          pricePerMonth >= minPrice &&
          pricePerMonth <= maxPrice &&
          numberOfRooms >= minRoom &&
          numberOfRooms <= maxRoom,
      ),
    );
  }, [
    apartments,
    maxPrice,
    maxRoom,
    maxSize,
    minPrice,
    minRoom,
    minSize,
    setFilteredApartments,
  ]);

  return (
    <Grid
      sx={{
        padding: 1,
      }}
    >
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        divider={<Divider orientation="vertical" flexItem />}
        spacing={3}
        // sx={{ background: "red" }}
      >
        <Typography variant="h6">Filter</Typography>
        <Stack direction="row" alignItems="center" spacing={1}>
          <Typography>Size</Typography>
          <TextField
            value={minSize}
            onChange={updateFilter(setMinSize)}
            {...defaultFilterInputProps}
          />
          <Typography>-</Typography>
          <TextField
            value={maxSize}
            onChange={updateFilter(setMaxSize)}
            {...defaultFilterInputProps}
          />
          <Typography>㎡</Typography>
        </Stack>
        <Stack direction="row" alignItems="center" spacing={1}>
          <Typography>Price</Typography>
          <TextField
            value={minPrice}
            onChange={updateFilter(setMinPrice)}
            {...defaultFilterInputProps}
          />
          <Typography>-</Typography>
          <TextField
            value={maxPrice}
            onChange={updateFilter(setMaxPrice)}
            {...defaultFilterInputProps}
          />
          <Typography>€</Typography>
        </Stack>
        <Stack direction="row" alignItems="center" spacing={1}>
          <Typography>Rooms</Typography>
          <TextField
            value={minRoom}
            onChange={updateFilter(setMinRoom)}
            {...defaultFilterInputProps}
          />
          <Typography>-</Typography>
          <TextField
            value={maxRoom}
            onChange={updateFilter(setMaxRoom)}
            {...defaultFilterInputProps}
          />
        </Stack>
        <Button onClick={onReset}>Reset</Button>
      </Stack>
    </Grid>
  );
}
