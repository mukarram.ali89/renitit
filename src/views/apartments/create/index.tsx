import { Button, Typography } from "@mui/material";
import { Box } from "@mui/system";
import * as React from "react";
import { useHistory } from "react-router-dom";
import { ApartmentCreationKeys, ApartmentCreationParams } from "../../../types";
import { APARTMENTS_ENDPOINT } from "../../../utils";
import { apiClient } from "../../../utils/apiClient";
import { ApartmentForm } from "../shared";

export function ApartmentCreate() {
  const history = useHistory();
  const [apartment, setApartment] = React.useState<ApartmentCreationParams>({
    areaSize: 0,
    description: "",
    floor: 0,
    geolocation: { lat: 0, lng: 0, title: "" },
    name: "",
    numberOfRooms: 0,
    pricePerMonth: 0,
    realtorId: 0,
  });
  const onChangeField = React.useCallback(
    (field: ApartmentCreationKeys, value: any) => {
      setApartment({
        ...(apartment || {}),
        [field]: value,
      });
    },
    [setApartment, apartment],
  );

  const onSubmit = React.useCallback(async () => {
    await apiClient.post(APARTMENTS_ENDPOINT, apartment);
    history.push("/apartments");
  }, [apartment, history]);

  return (
    <Box>
      <div>
        <Button onClick={() => history.push("/apartments")}>
          Back To Apartments
        </Button>
      </div>
      <Typography variant="h3">New Apartment</Typography>
      <Box>
        <ApartmentForm
          type="create"
          apartment={apartment}
          onChangeField={onChangeField}
        />
        <Button variant="contained" onClick={onSubmit}>
          Create
        </Button>
      </Box>
    </Box>
  );
}
