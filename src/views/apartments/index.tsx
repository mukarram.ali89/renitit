import { Grid } from "@mui/material";
import * as React from "react";
import {
  fetchApartments,
  selectApartments
} from "../../store/apartment/apartment.slice";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { ApartmentReadAttributes } from "../../types";
import { ApartmentsListContainer } from "./ApartmentListContainer";
import { ApartmentsMapContainer } from "./ApartmentsMapContainer";
import { Filter } from "./Filter";

export function Apartments() {
  const dispatch = useAppDispatch();

  const apartments = useAppSelector(selectApartments);
  const [filteredApartments, setFilteredApartments] = React.useState<
    ApartmentReadAttributes[] | null
  >(null);

  React.useEffect(() => {
    if (filteredApartments === null && apartments.length > 0) {
      setFilteredApartments(apartments);
    }
  }, [apartments, filteredApartments]);

  React.useEffect(() => {
    dispatch(fetchApartments());
  }, [dispatch]);

  const [selected, setSelected] = React.useState<number>(-1);

  return (
    <Grid>
      <Filter
        apartments={apartments}
        setFilteredApartments={setFilteredApartments}
      />
      <Grid container>
        <Grid item xs={3}>
          <ApartmentsListContainer
            selected={selected}
            setSelected={setSelected}
            apartments={filteredApartments || []}
          />
        </Grid>
        <Grid item xs={9}>
          <ApartmentsMapContainer
            selected={selected}
            setSelected={setSelected}
            apartments={filteredApartments || []}
          />
        </Grid>
      </Grid>
    </Grid>
  );
}
