import { Button, Typography } from "@mui/material";
import { Box } from "@mui/system";
import * as React from "react";
import { useHistory } from "react-router-dom";
import { UserCreationAttributes, UserCreationKeys } from "../../../types";
import { USERS_ENDPOINT } from "../../../utils";
import { apiClient } from "../../../utils/apiClient";
import { UserForm } from "../shared";

export function UserCreate() {
  const [user, setUser] = React.useState<UserCreationAttributes>({
    email: "",
    name: "",
    role: "client",
  });
  const history = useHistory();

  const onChangeField = React.useCallback(
    (field: UserCreationKeys, value: string) => {
      setUser({
        ...user,
        [field]: value,
      });
    },
    [setUser, user],
  );

  const onSubmit = React.useCallback(async () => {
    await apiClient.post(USERS_ENDPOINT, user);
    history.push("/users");
  }, [user, history]);

  return (
    <Box>
      <div>
        <Button onClick={() => history.push("/users")}>Back To Users</Button>
      </div>
      <Typography variant="h3">New User</Typography>
      <Box>
        <UserForm onChangeField={onChangeField} user={user} />
        <Button variant="contained" onClick={onSubmit}>
          Create
        </Button>
      </Box>
    </Box>
  );
}
