import { Button, Typography } from "@mui/material";
import { Box } from "@mui/system";
import * as React from "react";
import { useParams } from "react-router";
import { useHistory } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../store/hooks";
import { fetchUsers, selectUserById } from "../../../store/user/user.slice";
import { UserCreationKeys, UserReadAttributes } from "../../../types";
import { USERS_ENDPOINT } from "../../../utils";
import { apiClient } from "../../../utils/apiClient";
import { UserForm } from "../shared";

export function UserEdit() {
  const { id } = useParams<{ id: string }>();

  const dispatch = useAppDispatch();
  const history = useHistory();
  const selectedUser = useAppSelector(selectUserById(Number(id)));
  const [user, setUser] = React.useState<UserReadAttributes>({
    id: -1,
    email: "",
    name: "",
    role: "client",
  });

  React.useEffect(() => {
    if (user.id < 0 && selectedUser?.id) {
      setUser(selectedUser);
    }
  }, [user, selectedUser]);

  React.useEffect(() => {
    dispatch(fetchUsers());
  }, [dispatch]);

  const onChangeField = React.useCallback(
    (field: UserCreationKeys, value: string) => {
      setUser({
        ...user,
        [field]: value,
      });
    },
    [setUser, user],
  );

  const onSubmit = React.useCallback(async () => {
    const { id, createdAt, updatedAt, ...userData } =
      user as UserReadAttributes;
    await apiClient.patch(`${USERS_ENDPOINT}/${id}`, userData);
    history.push("/users");
  }, [user, history]);

  return (
    <Box>
      <div>
        <Button onClick={() => history.push("/users")}>Back To Users</Button>
      </div>
      <Typography variant="h3">Update User</Typography>
      <Box>
        <UserForm onChangeField={onChangeField} user={user} />
        <Button variant="contained" onClick={onSubmit}>
          Update
        </Button>
      </Box>
    </Box>
  );
}
