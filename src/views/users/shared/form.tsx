import { MenuItem, TextField, TextFieldProps } from "@mui/material";
import { Box } from "@mui/system";
import * as React from "react";
import { User, UserCreationKeys } from "../../../types";
import { roles } from "../../../utils";

interface FormProps {
  user: Partial<User>;
  onChangeField: (field: UserCreationKeys, value: any) => void;
}

export function UserForm(props: FormProps) {
  const { user, onChangeField } = props;
  const commonTextFieldProps: Partial<TextFieldProps> = {
    required: true,
    InputLabelProps: {
      shrink: true,
    },
    sx: {
      marginTop: 2,
      width: 300,
    },
  };
  return (
    <Box sx={{ mb: 1 }}>
      <div>
        <TextField
          {...commonTextFieldProps}
          label="Name"
          value={user.name}
          onChange={({ target: { value } }) => onChangeField("name", value)}
        />
      </div>
      <div>
        <TextField
          {...commonTextFieldProps}
          type="email"
          label="Email"
          value={user.email}
          onChange={({ target: { value } }) => onChangeField("email", value)}
        />
      </div>
      <div>
        <TextField
          {...commonTextFieldProps}
          label="Role"
          select
          value={user.role}
          onChange={({ target: { value } }) => onChangeField("role", value)}
        >
          {roles.map((role) => (
            <MenuItem key={role.value} value={role.value}>
              {role.label}
            </MenuItem>
          ))}
        </TextField>
      </div>
    </Box>
  );
}
