import AddCircleIcon from "@mui/icons-material/AddCircle";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import {
  Avatar,
  Box,
  Button,
  Container,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from "@mui/material";
import { grey } from "@mui/material/colors";
import * as React from "react";
import { useHistory } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import {
  fetchUsers,
  selectUsers,
  userActions,
} from "../../store/user/user.slice";
import { USERS_ENDPOINT } from "../../utils";
import { apiClient } from "../../utils/apiClient";

export function Users() {
  const dispatch = useAppDispatch();
  const history = useHistory();
  const users = useAppSelector(selectUsers);

  React.useEffect(() => {
    dispatch(fetchUsers());
  }, [dispatch]);

  const onDelete = React.useCallback(
    async (id: number) => {
      await apiClient.delete(`${USERS_ENDPOINT}/${id}`);
      dispatch(userActions.removeUser(id));
    },
    [dispatch],
  );

  return (
    <Container>
      <Box>
        <Box
          sx={{
            display: "flex",
            marginTop: 8,
            alignItems: "center",
            justifyContent: "space-between",
            borderBottom: `1px solid ${grey[500]}`,
          }}
        >
          <Typography variant="h6"> Create User</Typography>
          <Button onClick={() => history.push("/users/create")}>
            <AddCircleIcon
              sx={{
                fontSize: 40,
              }}
            />
          </Button>
        </Box>
        <List>
          {users.map((user) => (
            <ListItem key={user.id} sx={{ borderBottom: "1px solid #e7e7e7" }}>
              <ListItemText primary={user.name} secondary={user.role} />
              <ListItemAvatar>
                <Avatar sx={{ bgcolor: grey[200] }}>
                  <Button
                    onClick={() => history.push(`/users/edit/${user.id}`)}
                  >
                    <EditIcon />
                  </Button>
                </Avatar>
              </ListItemAvatar>
              <IconButton onClick={() => onDelete(user.id)}>
                <DeleteIcon />
              </IconButton>
            </ListItem>
          ))}
        </List>
      </Box>
    </Container>
  );
}
