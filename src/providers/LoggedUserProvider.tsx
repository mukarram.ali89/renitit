import * as React from "react";
import { UserReadAttributes } from "../types";

const NavbarOptionContext = React.createContext<UserReadAttributes | null>(
  null
);

const NavbarOptionSetterContext = React.createContext<
  React.Dispatch<React.SetStateAction<UserReadAttributes | null>>
>(
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  () => {}
);

export const useLoggedUser = () => {
  return React.useContext(NavbarOptionContext);
};

export const useLoggedUserSetter = () => {
  return React.useContext(NavbarOptionSetterContext);
};

export const LoggedUserProvider = ({
  children,
}: {
  children: React.ReactElement;
}) => {
  const [navbarOptions, setLoggedUser] =
    React.useState<UserReadAttributes | null>(null);
  return (
    <NavbarOptionSetterContext.Provider value={setLoggedUser}>
      <NavbarOptionContext.Provider value={navbarOptions}>
        {children}
      </NavbarOptionContext.Provider>
    </NavbarOptionSetterContext.Provider>
  );
};
