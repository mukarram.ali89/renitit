import * as React from "react";

interface NavbarOptionsProps {
  login?: boolean;
  register?: boolean;
  user?: boolean;
  apartment?: boolean;
  logout?: boolean;
}
const NavbarOptionContext = React.createContext<NavbarOptionsProps>({});

const NavbarOptionSetterContext = React.createContext<
  React.Dispatch<React.SetStateAction<NavbarOptionsProps>>
>(
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  () => {}
);

export const useNavbarOptions = () => {
  return React.useContext(NavbarOptionContext);
};

export const useNavbarOptionsSetter = () => {
  return React.useContext(NavbarOptionSetterContext);
};

export const NavbarOptionsProvider = ({
  children,
}: {
  children: React.ReactElement;
}) => {
  const [navbarOptions, setNavbarOptions] = React.useState<NavbarOptionsProps>({
    apartment: true
  });
  return (
    <NavbarOptionSetterContext.Provider value={setNavbarOptions}>
      <NavbarOptionContext.Provider value={navbarOptions}>
        {children}
      </NavbarOptionContext.Provider>
    </NavbarOptionSetterContext.Provider>
  );
};
