import { Box } from "@mui/system";
import * as React from "react";
import { Provider as ReduxProvider } from 'react-redux';
import { store } from '../store';
import { LoggedUserProvider } from "./LoggedUserProvider";
import { NavbarOptionsProvider } from "./NavbarOptionsProvider";

export default function Provider({
  children,
}: {
  children: React.ReactElement;
}) {
  return (
    <Box>
      <ReduxProvider store={store}>
        <LoggedUserProvider>
          <NavbarOptionsProvider>{children}</NavbarOptionsProvider>
        </LoggedUserProvider>
      </ReduxProvider>
    </Box>
  );
}
