export type roles = "client" | "admin" | "realtor";

export interface User {
  id: number;
  email: string;
  name: string;
  role: roles;
  hashedPassword: string;
}

export interface UserReadAttributes extends Omit<User, "hashedPassword"> {
  createdAt?: Date;
  updatedAt?: Date;
}

export type UserCreationKeys = "name" | "email" | "role";

export interface UserCreationAttributes
  extends Omit<User, "hashedPassword" | "id"> {}
