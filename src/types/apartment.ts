export interface TGeolocation {
  lat: number;
  lng: number;
  title?: string;
}

export interface Apartment {
  id: number;
  areaSize: number;
  available: boolean;
  dateAdded: Date;
  description: string;
  floor: number;
  geolocation: TGeolocation;
  name: string;
  numberOfRooms: number;
  pricePerMonth: number;
  realtorId: number;
}

export interface ApartmentCreationParams
  extends Omit<Apartment, "id" | "dateAdded" | "available"> {}

export interface ApartmentReadAttributes extends Apartment {
  createdAt?: Date;
  updatedAt?: Date;
}

export type ApartmentCreationKeys =
  | "areaSize"
  | "available"
  | "dateAdded"
  | "description"
  | "floor"
  | "geolocation"
  | "name"
  | "numberOfRooms"
  | "pricePerMonth"
  | "realtorId";

export type ApartmentUpdationKeys = ApartmentCreationKeys | "id";
