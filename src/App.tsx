import { Grid } from "@mui/material";
import React from "react";
import { BrowserRouter } from "react-router-dom";
import { Navbar } from "./components";
import { AuthSetter } from "./components/auth";
import Provider from "./providers";
import { Routes } from "./views/routes";

function App() {
  return (
    <Provider>
      <Grid className="App">
        <AuthSetter>
          <BrowserRouter>
            <Navbar />
            <Routes />
          </BrowserRouter>
        </AuthSetter>
      </Grid>
    </Provider>
  );
}

export default App;
