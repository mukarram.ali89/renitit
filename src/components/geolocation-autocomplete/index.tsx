import LocationOnIcon from "@mui/icons-material/LocationOn";
import Autocomplete from "@mui/material/Autocomplete";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import parse from "autosuggest-highlight/parse";
import throttle from "lodash/throttle";
import * as React from "react";
import { TGeolocation } from "../../types";
import { GOOGLE_MAPS_KEY } from "../../utils";
import {
  GeocoderResult,
  geolocationFromGeocoderResult,
  googleMapsService,
  loadScript,
  PlaceType,
} from "./utils";

interface Props {
  defaultTitle?: string;
  setCoordinates: (g: TGeolocation) => void;
}

export function GeolocationAutocomplete({
  defaultTitle,
  setCoordinates,
}: Props) {
  const [place, setPlace] = React.useState<PlaceType | null>(null);
  const [query, setQuery] = React.useState("");
  const [suggestions, setSuggestions] = React.useState<readonly PlaceType[]>(
    [],
  );
  const loaded = React.useRef(false);

  if (typeof window !== "undefined" && !loaded.current) {
    if (!document.querySelector("#google-maps")) {
      loadScript(
        `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_MAPS_KEY}&libraries=places`,
        document.querySelector("head"),
        "google-maps",
      );
    }

    loaded.current = true;
  }

  const fetchSuggestions = React.useMemo(
    () =>
      throttle(
        (
          request: { input: string },
          callback: (results?: readonly PlaceType[]) => void,
        ) => {
          (googleMapsService.places.current as any).getPlacePredictions(
            request,
            callback,
          );
        },
        200,
      ),
    [],
  );

  const fetchGeolocation = React.useMemo(
    () => (placeId: string, callback: (results?: GeocoderResult[]) => void) => {
      (googleMapsService.geocoder.current as any).geocode(
        { placeId },
        callback,
      );
    },
    [],
  );

  /**
   * In case of update, title is received as prop which is initially undefined
   * Which is why useState initialization isn't enough
   */
  React.useEffect(() => {
    const defaultValues =
      defaultTitle === undefined
        ? null
        : ({ description: defaultTitle } as PlaceType);
    if (defaultTitle) {
      setPlace(defaultValues);
    }
  }, [defaultTitle]);

  /**
   * Fetch suggestions for types query
   */
  React.useEffect(() => {
    let active = true;

    const googleLib = (window as any).google;
    if (!googleMapsService.places.current && googleLib && googleLib?.maps) {
      googleMapsService.places.current =
        new googleLib.maps.places.AutocompleteService();
    }
    if (!googleMapsService.places.current) {
      return undefined;
    }

    if (query === "") {
      setSuggestions(place ? [place] : []);
      return undefined;
    }

    fetchSuggestions({ input: query }, (results?: readonly PlaceType[]) => {
      if (active) {
        let newOptions: readonly PlaceType[] = [];
        if (place) {
          newOptions = [place];
        }
        if (results) {
          newOptions = [...newOptions, ...results];
        }
        setSuggestions(newOptions);
      }
    });

    return () => {
      active = false;
    };
  }, [place, query, fetchSuggestions]);

  /**
   * Fetched coordinates of selected suggestion
   */
  React.useEffect(() => {
    let active = true;
    if (!googleMapsService.geocoder.current && (window as any).google) {
      googleMapsService.geocoder.current = new (
        window as any
      ).google.maps.Geocoder();
    }
    if (!googleMapsService.geocoder.current) {
      return undefined;
    }

    const placeId = place?.place_id || "";
    if (placeId === "") {
      return;
    }

    fetchGeolocation(placeId, (results) => {
      if (active && results !== undefined) {
        setCoordinates(geolocationFromGeocoderResult(results[0]));
      }
    });

    return () => {
      active = false;
    };
  }, [place, fetchGeolocation, setCoordinates]);

  return (
    <Autocomplete
      id="google-map-demo"
      sx={{ width: 300, mt: 2 }}
      getOptionLabel={(option) =>
        typeof option === "string" ? option : option.description
      }
      filterOptions={(x) => x}
      options={suggestions}
      autoComplete
      includeInputInList
      filterSelectedOptions
      value={place}
      onChange={(_, newValue: PlaceType | null) => {
        setSuggestions(newValue ? [newValue, ...suggestions] : suggestions);
        setPlace(newValue);
      }}
      onInputChange={(_, newInputValue) => {
        setQuery(newInputValue);
      }}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Add a location"
          defaultValue={defaultTitle}
          fullWidth
        />
      )}
      renderOption={(props, option) => {
        const matches =
          option.structured_formatting.main_text_matched_substrings;
        const parts = parse(
          option.structured_formatting.main_text,
          matches.map((match: any) => [
            match.offset,
            match.offset + match.length,
          ]),
        );

        return (
          <li {...props}>
            <Grid container alignItems="center">
              <Grid item>
                <Box
                  component={LocationOnIcon}
                  sx={{ color: "text.secondary", mr: 2 }}
                />
              </Grid>
              <Grid item xs>
                {parts.map((part, index) => (
                  <span
                    key={index}
                    style={{
                      fontWeight: part.highlight ? 700 : 400,
                    }}
                  >
                    {part.text}
                  </span>
                ))}
                <Typography variant="body2" color="text.secondary">
                  {option.structured_formatting.secondary_text}
                </Typography>
              </Grid>
            </Grid>
          </li>
        );
      }}
    />
  );
}
