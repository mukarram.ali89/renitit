import { TGeolocation } from "../../types";

interface MainTextMatchedSubstrings {
  offset: number;
  length: number;
}
interface StructuredFormatting {
  main_text: string;
  secondary_text: string;
  main_text_matched_substrings: readonly MainTextMatchedSubstrings[];
}
export interface PlaceType {
  description: string;
  place_id: string;
  structured_formatting: StructuredFormatting;
}

export interface GeocoderResult {
  formatted_address: string;
  geometry: {
    location: {
      lat: () => number;
      lng: () => number;
    };
  };
}

export function geolocationFromGeocoderResult({
  geometry: {
    location: { lat, lng },
  },
  formatted_address: title,
}: GeocoderResult): TGeolocation {
  return { lat: lat(), lng: lng(), title };
}

export async function geocodeBySuggestion(query: any) {
  return {
    lat: 0,
    lng: 0,
  };
}

export function loadScript(
  src: string,
  position: HTMLElement | null,
  id: string,
) {
  if (!position) {
    return;
  }

  const script = document.createElement("script");
  script.setAttribute("async", "");
  script.setAttribute("id", id);
  script.src = src;
  position.appendChild(script);
}

export const googleMapsService = {
  places: {
    current: null,
  },
  geocoder: {
    current: null,
  },
};
