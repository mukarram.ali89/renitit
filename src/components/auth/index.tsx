import * as React from "react";
import { useNavbarOptions, useNavbarOptionsSetter } from "../../providers/NavbarOptionsProvider";
import { authActions, LoginResponse, selectUser } from "../../store/auth/auth.slice";
import { useAppDispatch, useAppSelector } from "../../store/hooks";

export function saveSession({token, user}: LoginResponse) {
  localStorage.setItem('token', token);
  localStorage.setItem('user', JSON.stringify(user));
}

export function deleteSession() {
  localStorage.removeItem('token');
  localStorage.removeItem('user');
}

export function getSession(): LoginResponse {
  return {
    token: localStorage.getItem('token') || '',
    user: JSON.parse(localStorage.getItem('user') || '{}') as LoginResponse['user'],
  }
}

interface Props {

}
export function AuthSetter({children}: React.PropsWithChildren<Props>) {
  const dispatch = useAppDispatch();
  const loggedUser = useAppSelector(selectUser);
  const navbarOptions = useNavbarOptions();
  const setNavbarOptions = useNavbarOptionsSetter();

  React.useEffect(() => {
    const { token, user } = getSession();
    if(token && user.email) {
      dispatch(authActions.setUserAndToken({ token, user }));
    }
  }, [dispatch])

  React.useEffect(() => {
    setNavbarOptions({
      ...navbarOptions,
      register: !loggedUser,
      login: !loggedUser,
      user: loggedUser?.role === 'admin',
      logout: !!loggedUser,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loggedUser, setNavbarOptions]);

  return <>{children}</>;
}