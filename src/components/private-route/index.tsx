import { CircularProgress } from "@mui/material";
import React from "react";
import { Redirect, Route, RouteProps } from "react-router-dom";
import { selectAuthLoading, selectUser } from "../../store/auth/auth.slice";
import { useAppSelector } from "../../store/hooks";
import { roles } from "../../types";

interface PrivateRouteProps extends RouteProps {
  roles: roles[]
}
export const PrivateRoute = ({
  children,
  roles,
  ...rest
}: React.PropsWithChildren<PrivateRouteProps>) => {
  const user = useAppSelector(selectUser);
  const isAuthLoading = useAppSelector(selectAuthLoading);
  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthLoading ? (
          <CircularProgress sx={{ height: 100 }} />
        ) : roles.includes(user?.role as roles) ? (
          children
        ) : (
          <Redirect
            to={{ pathname: "/login", state: { from: props.location } }}
          />
        )
      }
    />
  );
};
