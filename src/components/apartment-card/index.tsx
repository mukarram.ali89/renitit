import Delete from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import {
  Box,
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardHeader,
  Collapse,
  Typography,
} from "@mui/material";
import { styled } from "@mui/system";
import * as React from "react";
import { useHistory } from "react-router-dom";
import { apartmentActions } from "../../store/apartment/apartment.slice";
import { useAppDispatch } from "../../store/hooks";
import { Apartment } from "../../types";
import { APARTMENTS_ENDPOINT } from "../../utils";
import { apiClient } from "../../utils/apiClient";

interface Props {
  apartment: Apartment;
  expanded: boolean;
  hasWritePermissions: boolean;
  onSelect: () => void;
}

const ApartmentDeleteIcon = styled(Delete)({
  cursor: "pointer",
});

const RentedButton = styled(Typography)({
  cursor: "pointer",
  color: "red",
});

export function ApartmentCard({
  apartment,
  expanded,
  onSelect,
  hasWritePermissions: writePermissions = false,
}: Props) {
  const properties = [
    {
      key: "Address",
      value: apartment.geolocation.title,
    },
    {
      key: "Area Size",
      value: apartment.areaSize,
    },
    {
      key: "DateAdded",
      value: new Date(apartment.dateAdded).toDateString(),
    },
    {
      key: "Rooms",
      value: apartment.numberOfRooms,
    },
    {
      key: "Floor",
      value: apartment.floor,
    },
    {
      key: "Realtor",
      value: apartment.realtorId,
    },
  ];
  const { id, available } = apartment;
  const dispatch = useAppDispatch();
  const history = useHistory();

  const onDelete = React.useCallback(async () => {
    await apiClient.delete(`${APARTMENTS_ENDPOINT}/${id}`);
    dispatch(apartmentActions.removeApartment(id));
  }, [dispatch, id]);

  const rentedHandler = React.useCallback(async () => {
    await apiClient.patch(`${APARTMENTS_ENDPOINT}/${id}`, {
      available: !available,
    });
    dispatch(
      apartmentActions.updateApartment({
        id,
        changes: {
          available: !available,
        },
      }),
    );
  }, [id, available, dispatch]);

  return (
    <Card onClick={onSelect}>
      <Box sx={{ display: "flex" }}>
        <CardActionArea>
          <CardHeader
            title={apartment.name}
            subheader={`${apartment.pricePerMonth}$/month`}
          />
        </CardActionArea>
        {writePermissions && (
          <CardActions
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Button
              onClick={() => history.push(`/apartments/edit/${apartment.id}`)}
            >
              <EditIcon />
            </Button>
            <ApartmentDeleteIcon onClick={onDelete} />
            <RentedButton onClick={rentedHandler} variant="button">
              {apartment.available ? "RENTED" : "AVAILABLE"}
            </RentedButton>
          </CardActions>
        )}
      </Box>
      <Collapse in={expanded} unmountOnExit>
        <CardContent>
          <Typography variant="body2">
            More Info: {apartment.description}
          </Typography>
        </CardContent>
        <CardContent>
          <Typography variant="body2" sx={{ whiteSpace: "pre-line" }}>
            {properties.map(({ key, value }) => `${key}: ${value}\n`)}
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}
