import { AppBar, Button, Toolbar, Typography } from "@mui/material";
import { Box } from "@mui/system";
import * as React from "react";
import { useHistory } from "react-router-dom";
import { useNavbarOptions } from "../../providers/NavbarOptionsProvider";
import { authActions } from "../../store/auth/auth.slice";
import { useAppDispatch } from "../../store/hooks";
import { deleteSession } from "../auth";

export function Navbar() {
  const dispatch = useAppDispatch();
  const history = useHistory();
  const { apartment, login, register, user, logout } = useNavbarOptions();

  const logoutHandler = React.useCallback(() => {
    deleteSession();
    dispatch(authActions.resetAuthAndToken());
  }, [dispatch]);

  const navigateTo = React.useCallback(
    (to: string) => {
      history.push(to);
    },
    [history],
  );

  return (
    <Box
      sx={{
        marginBottom: 6,
      }}
    >
      <AppBar>
        <Toolbar variant="dense">
          <Typography variant="h5">RentIT</Typography>
          <Box
            sx={{
              marginLeft: 5,
              width: "25%",
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            {apartment && (
              <Button color="inherit" onClick={() => navigateTo("/apartments")}>
                Apartments
              </Button>
            )}
            {user && (
              <Button color="inherit" onClick={() => navigateTo("/users")}>
                Users
              </Button>
            )}
            {register && (
              <Button color="inherit" onClick={() => navigateTo("/register")}>
                Register
              </Button>
            )}
            {login && (
              <Button color="inherit" onClick={() => navigateTo("/login")}>
                Login
              </Button>
            )}
            {logout && (
              <Button color="inherit" onClick={logoutHandler}>
                Logout
              </Button>
            )}
          </Box>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
