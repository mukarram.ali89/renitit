import GoogleMapReact from "google-map-react";
import { Apartment } from "../../types";
import { GOOGLE_MAPS_KEY } from "../../utils";
import MapMarker from "./MapMarker";

interface ApartmentsMapProps {
  apartments: Apartment[];
  selected: number;
  setSelected: React.Dispatch<React.SetStateAction<number>>;
}

const ApartmentsMap = ({
  apartments,
  selected,
  setSelected,
}: ApartmentsMapProps) => (
  <div style={{ height: "100vh", width: "100%" }}>
    <GoogleMapReact
      bootstrapURLKeys={{
        key: GOOGLE_MAPS_KEY,
      }}
      center={apartments?.[0]?.geolocation}
      defaultZoom={11}
    >
      {apartments
        .filter(
          (apartment) =>
            !!apartment.geolocation.lat && !!apartment.geolocation.lng,
        )
        .map(({ id, geolocation: { lat, lng } }) => (
          <MapMarker
            onSelect={() =>
              id === selected ? setSelected(-1) : setSelected(id)
            }
            focused={selected === id}
            lat={lat}
            lng={lng}
            key={id}
            id={id}
          />
        ))}
    </GoogleMapReact>
  </div>
);

export default ApartmentsMap;
