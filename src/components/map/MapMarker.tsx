import { PinDrop, Room } from "@mui/icons-material";
import { IconButton } from "@mui/material";
import React from "react";

export interface MarkerProps {
  id: number;
  lat: number;
  lng: number;
  onSelect: () => void;
  focused: boolean;
}

const MapMarker = (props: MarkerProps) => {
  const { id, onSelect, focused } = props;
  return (
    <IconButton size="large" key={id} onClick={onSelect}>
      {focused ? <PinDrop color="success" /> : <Room color="warning" />}
    </IconButton>
  );
};

export default MapMarker;
