import { roles } from "../types";

export const ACCESS = {
  APARTMENTS: {
    WRITE: ['realtor', 'admin'] as roles[],
  },
  USERS: {
    WRITE: ['admin'] as roles[],
    READ: ['admin'] as roles[],
  }
}