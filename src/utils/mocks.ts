import { Apartment, UserReadAttributes } from "../types";

export const realtors: UserReadAttributes[] = [
  {
    id: 1,
    name: "Realtor1",
  } as UserReadAttributes,
];

export const apartments: Apartment[] = [
  {
    id: 1,
    available: true,
    areaSize: 1000,
    dateAdded: new Date(),
    description: "Nothing here",
    floor: 1,
    geolocation: {
      lat: 51.227741,
      lng: 6.773456,
      title: "Dusseldorf 49",
    },
    name: "A B",
    numberOfRooms: 3,
    pricePerMonth: 1200,
    realtorId: 1,
  },
];
