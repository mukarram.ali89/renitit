export const LOGIN_ENDPOINT = "login";
export const REGISTER_ENDPOINT = "register";
export const APARTMENTS_ENDPOINT = "apartments";
export const USERS_ENDPOINT = "users";
