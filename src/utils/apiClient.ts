import axios from "axios";
import { API_URL } from ".";
import { getSession } from "../components/auth";

export const apiClient = axios.create({
  baseURL: API_URL,
  timeout: 5000,
  headers: {
    Authorization: `Bearer ${getSession().token}`,
  },
});
