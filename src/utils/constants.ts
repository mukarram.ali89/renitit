export const roles = [
  {
    value: "client",
    label: "Client",
  },
  {
    value: "realtor",
    label: "Realtor",
  },
  {
    value: "admin",
    label: "Admin",
  },
];
