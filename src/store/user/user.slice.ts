import {
  createAsyncThunk,
  createEntityAdapter,
  createSelector,
  createSlice,
  EntityState,
} from "@reduxjs/toolkit";
import { UserReadAttributes } from "../../types";
import { USERS_ENDPOINT } from "../../utils";
import { apiClient } from "../../utils/apiClient";

export const USERS_SLICE_KEY = "users";

const usersAdapter = createEntityAdapter<UserReadAttributes>({
  selectId: (user) => user.id,
  sortComparer: (a, b) => a.name.localeCompare(b.name),
});

export const fetchUsers = createAsyncThunk("users/fetch", async () => {
  return (
    await apiClient.get<any, { data: UserReadAttributes[] }>(USERS_ENDPOINT)
  ).data;
});

const initialState = usersAdapter.getInitialState();

const usersSlice = createSlice({
  name: "users",
  initialState,
  reducers: {
    removeUser: usersAdapter.removeOne,
    updateUser: usersAdapter.updateOne,
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchUsers.fulfilled, (state, { payload }) => {
        usersAdapter.setAll(state, payload);
      })
      .addDefaultCase((state) => state);
  },
});

export const selectUsersState = (
  rootState: any,
): EntityState<UserReadAttributes> => rootState[USERS_SLICE_KEY];

const { selectAll, selectById } = usersAdapter.getSelectors();

export const selectUsers = createSelector(selectUsersState, selectAll);

export const selectUserById = (id: number) =>
  createSelector(selectUsersState, (state) => selectById(state, id));

export const { reducer: userReducer, actions: userActions } = usersSlice;
