import { createSlice } from "@reduxjs/toolkit";
import type { RootState } from "..";
import { UserReadAttributes } from "../../types";

interface AuthState {
  user: UserReadAttributes | null;
  session: {
    token: string | null;
  };
  loading: boolean;
}

export interface LoginResponse {
  token: string;
  user: UserReadAttributes;
}

// Define the initial state using that type
const initialState: AuthState = {
  user: null,
  session: {
    token: null,
  },
  loading: true,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setUserAndToken: (
      state,
      { payload: { token, user } }: { payload: LoginResponse }
    ) => {
      state.user = user;
      state.session.token = token;
      state.loading = false;
    },
    resetAuthAndToken: (state) => {
      state.user = null;
      state.session.token = null;
    },
    setAuthLoading: (
      state,
      { payload: { loading } }: { payload: { loading: boolean } }
    ) => {
      state.loading = loading;
    },
  },
});

export const selectUser = (state: RootState) => state.auth.user;
export const selectToken = (state: RootState) => state.auth.session.token;
export const selectAuthLoading = (state: RootState) => state.auth.loading;

export const { reducer: authReducer, actions: authActions } = authSlice;
