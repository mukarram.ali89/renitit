import { configureStore } from "@reduxjs/toolkit";
import { apartmentReducer, APARTMENTS_SLICE_KEY } from "./apartment/apartment.slice";
import { authReducer } from "./auth/auth.slice";
import { userReducer, USERS_SLICE_KEY } from "./user/user.slice";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    [APARTMENTS_SLICE_KEY]: apartmentReducer,
    [USERS_SLICE_KEY]: userReducer
  },
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
