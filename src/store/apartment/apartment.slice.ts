import {
  createAsyncThunk,
  createEntityAdapter,
  createSelector,
  createSlice,
  EntityState,
} from "@reduxjs/toolkit";
import { ApartmentReadAttributes } from "../../types";
import { APARTMENTS_ENDPOINT } from "../../utils";
import { apiClient } from "../../utils/apiClient";

export const APARTMENTS_SLICE_KEY = "apartments";

const apartmentsAdapter = createEntityAdapter<ApartmentReadAttributes>({
  selectId: (apartment) => apartment.id,
  sortComparer: (a, b) => a.name.localeCompare(b.name),
});

export const fetchApartments = createAsyncThunk(
  "apartments/fetch",
  async () => {
    return (
      await apiClient.get<any, { data: ApartmentReadAttributes[] }>(
        APARTMENTS_ENDPOINT,
      )
    ).data;
  },
);

const initialState = apartmentsAdapter.getInitialState();

const apartmentsSlice = createSlice({
  name: "apartments",
  initialState,
  reducers: {
    removeApartment: apartmentsAdapter.removeOne,
    updateApartment: apartmentsAdapter.updateOne,
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchApartments.fulfilled, (state, { payload }) => {
        apartmentsAdapter.setAll(state, payload);
      })
      .addDefaultCase((state) => state);
  },
});

export const selectApartmentsState = (
  rootState: any,
): EntityState<ApartmentReadAttributes> => rootState[APARTMENTS_SLICE_KEY];

const { selectAll, selectById } = apartmentsAdapter.getSelectors();

export const selectApartments = createSelector(
  selectApartmentsState,
  selectAll,
);

export const selectApartmentById = (id: number) =>
  createSelector(selectApartmentsState, (state) => selectById(state, id));

export const { reducer: apartmentReducer, actions: apartmentActions } =
  apartmentsSlice;
